﻿using DataProvider.Abstract;


namespace DataProvider.Impl;

public class FileDataProvider : IDataProvider<string>
{
    private readonly string _path;

    public FileDataProvider(string path)
    {
        _path = path;
    }

    public async Task<IEnumerable<string>> ProvideAsync()
    {
        if (string.IsNullOrEmpty(_path?.Trim()) && !File.Exists(_path))
            return Array.Empty<string>();

        return await File.ReadAllLinesAsync(_path);
    }
}
