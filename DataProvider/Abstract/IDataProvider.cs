﻿namespace DataProvider.Abstract;

interface IDataProvider<T>
{
    Task<IEnumerable<T>> ProvideAsync();
}
