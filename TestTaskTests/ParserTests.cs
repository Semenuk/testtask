﻿using Parser.Impl;
using LineParser = Parser.Impl.Parser;

namespace TestTaskTests
{
    public class ParserTests
    {
        [Test]
        [Parallelizable]
        public async Task TakeValidMaxRowNumber()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(new List<string> { "10,20,262.3,48", "dfs2wf262.3,48", "10,20,-22.3,8", "", "109,250,22.3,8" }, collector);

            var result = await parser.GetMaxValueLineNumber();
            
            Assert.That(result, Is.EqualTo(4));
        }


        [Test]
        [Parallelizable]
        public async Task TakeValidMaxRowNumber_OnlyNegativeNumbers()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(new List<string> { "-1,-2", "-8", "-3" }, collector);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        [Parallelizable]
        public async Task ReturnMinusOne_EmptySource()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(new List<string> { "" }, collector);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(-1));
        }

        [Test]
        [Parallelizable]
        public async Task ReturnMinusOne_AllBadLines_NoNumbers()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(new List<string> { "asdsa", "dfffdf" }, collector);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(-1));
        }

        [Test]
        [Parallelizable]
        public async Task TakeFirstMaxRowNumber_TwoTheSameRows()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(new List<string> { "109,250,22.3,8", "10,20,262.3,48", "dfs2wf262.3,48", "10,20,-22.3,8", "", "109,250,22.3,8" }, collector);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        [Parallelizable]
        public async Task InvalidLineNumber_EmptySource()
        {
            var collector = new BadLineCollector("");
            var parser = new LineParser(null, collector);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(-1));
        }

        [Test]
        [Parallelizable]
        public async Task ValidLineNumber_CollectorIsNull()
        {
            var parser = new LineParser(new List<string> { "10,20,262.3,48" }, null);

            var result = await parser.GetMaxValueLineNumber();

            Assert.That(result, Is.EqualTo(0));
        }
    }
}
