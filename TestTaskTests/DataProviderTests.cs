﻿using DataProvider.Impl;

namespace TestTaskTests
{
    internal class DataProviderIntegrationTests
    {
        [Test]
        [Parallelizable]
        public async Task AllLinesReadFromFile()
        {
            var fullFileName = Path.GetTempFileName();
            await File.WriteAllLinesAsync(fullFileName, new List<string> { "109,250,22.3,8", "10,20,262.3,48", "dfs2wf262.3,48", "10,20,-22.3,8", "", "109,250,22.3,8" });

            var dataProvider = new FileDataProvider(fullFileName);
            var result = await dataProvider.ProvideAsync();


            Assert.That(result.Count(), Is.EqualTo(6));
        }

        [Test]
        [Parallelizable]
        public async Task EmptyResult_FileDoesNotExist()
        {
            var dataProvider = new FileDataProvider(null);
            var result = await dataProvider.ProvideAsync();


            Assert.That(result.Count(), Is.EqualTo(0));
        }
    }
}
