using Parser;
using Parser.Impl;

namespace TestTaskTests
{
    public class BadLineCollectorIntegrationTests
    {
        [Test]
        [Parallelizable]
        public void ApplyDefaultPattern()
        {
            var collector = new BadLineCollector("");
            
            collector.Collect(new LineInfo<string>(1, "10,20,-262.3,48"));
            var result = collector.Items;

            Assert.That(result.Count(), Is.EqualTo(0));
        }

        [Test]
        [Parallelizable]
        public void CollectAllInvalidLines()
        {
            var collector = new BadLineCollector("");
            var source = new List<LineInfo<string>> 
            {
                new LineInfo<string>(0, "10,20,262.3,48"),
                new LineInfo<string>(1, "dfs2wf262.3,48"),
                new LineInfo<string>(2, "10,20,-22.3,8"),
                new LineInfo<string>(3, ""),
                new LineInfo<string>(4, null)
            };

            foreach (var line in source)
            {
                collector.Collect(line);
            }
            var result = collector.Items;

            Assert.That(result.Count(), Is.EqualTo(3));
        }

        [Test]
        [Parallelizable]
        public void ApplyCustomPattern()
        {
            var collector = new BadLineCollector(@"[^,\d]+");

            collector.Collect(new LineInfo<string>(1, "10,20,-262.3,48"));
            var result = collector.Items;

            Assert.That(result.Count(), Is.EqualTo(1));
        }
    }
}