﻿using System.Runtime.CompilerServices;

namespace Parser
{
    //[assembly: InternalsVisibleToAttribute("TestTaskTests")]
    public class LineInfo<T>
    {
        public LineInfo(int lineNumber, T value)
        {
            LineNumber = lineNumber;
            Value = value;
        }

        public int LineNumber { get; }
        public T Value { get; }
    }
}
