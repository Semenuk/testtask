﻿using Parser.Abstract;
using System.Text.RegularExpressions;


namespace Parser.Impl;

public class BadLineCollector : ICollector<LineInfo<string>, int>
{
    private const string DefaultValidatePattern = @"[^\s,\d.-]+";
    private List<int> _badLines = new List<int>();
    private readonly string _filterPattern;

    public BadLineCollector(string filterPattern)
    {
        _filterPattern = !string.IsNullOrWhiteSpace(filterPattern?.Trim()) ? filterPattern : DefaultValidatePattern;
    }

    public IEnumerable<int> Items => _badLines;

    public void Collect(LineInfo<string> lineInfo)
    {
        if (string.IsNullOrWhiteSpace(lineInfo.Value) || !IsValid(lineInfo.Value))
            _badLines.Add(lineInfo.LineNumber);
    }

    private bool IsValid(string line) =>
        Regex.Matches(line, _filterPattern).Count == 0;
}
