﻿using System.Globalization;
using System.Text.RegularExpressions;
using ICollector = Parser.Abstract.ICollector<Parser.LineInfo<string>, int>;


namespace Parser.Impl;

public class Parser
{
    private static string NumberPattern = @"-?\d+(?:\.\d+)?";
    private readonly string[] _data;
    private readonly ICollector _collector;

    public Parser(IEnumerable<string> data, ICollector collector)
    {
        _data = data is null ? Array.Empty<string>() : data.ToArray();
        _collector = collector;
    }

    public async Task<int> GetMaxValueLineNumber()
    {
        var lineNumber = await Task.Factory.StartNew(Process).ContinueWith(t => FindMaxValue(t.Result));
        return lineNumber;
    }

    private IEnumerable<LineInfo<double>> Process()
    {
        var lines = new List<LineInfo<double>>();

        for (var i = 0; i < _data.Length; i++)
        {
            var lineInfo = ProcessLine(_data[i], i);
            if (lineInfo is not null)
                lines.Add(lineInfo);
        }

        return lines;
    }

    private LineInfo<double> ProcessLine(string line, int lineNumber)
    {
        _collector?.Collect(new LineInfo<string>(lineNumber, line));

        var lineInfo = GetLineInfo(line, lineNumber);
        if (lineInfo is not null)
            return lineInfo;

        return null;
    }

    private LineInfo<double> GetLineInfo(string source, int lineNumber)
    {
        var rowValue = 0.0d;
        var numbers = Regex.Matches(source, NumberPattern);
        if (numbers.Count == 0)
            return null;

        for (var i = 0; i < numbers.Count; i++)
            rowValue += double.Parse(numbers[i].Value, new NumberFormatInfo());

        var lineInfo = new LineInfo<double>(lineNumber, rowValue);
        return lineInfo;
    }

    private int FindMaxValue(IEnumerable<LineInfo<double>> source)
    {
        const int InvalidLineIndex = -1;
        if (source.Count() == 0)
            return InvalidLineIndex;

        var lineNumber = InvalidLineIndex;
        var maxValue = double.MinValue;

        foreach (var item in source)
            if (item.Value > maxValue)
            {
                maxValue = item.Value;
                lineNumber = item.LineNumber;
            }

        return lineNumber;
    }
}
