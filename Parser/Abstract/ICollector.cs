﻿namespace Parser.Abstract;

public interface ICollector<T, E>
{
    IEnumerable<E> Items { get; }
    void Collect(T item);
}
