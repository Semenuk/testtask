﻿using DataProvider.Impl;
using Parser.Impl;
using LineParser = Parser.Impl.Parser;

internal class Program
{
    static async Task<IEnumerable<string>> TryTakeValidData(string source)
    {
        do
        {
            var data = await new FileDataProvider(source).ProvideAsync();
            if (data.Count() != 0)
                return data;

            Console.WriteLine("Please, set correct path, press 'q' or 'empty string' to exit");
            source = Console.ReadLine();
        } while (source != "q" || string.IsNullOrEmpty(source));

        return Array.Empty<string>();
    }

    static void PrintBadLines(BadLineCollector collector)
    {
        Console.WriteLine("Rows with not valid symbols:");
        foreach (var line in collector.Items)
            Console.WriteLine(line);
    }

    static async Task Main(string[] args)
    {
        IEnumerable<string> data = default;
        if (args.Length > 0)
            data = await TryTakeValidData(args[0]);
        else
        {
            Console.WriteLine("Please, set the path");
            var source = Console.ReadLine();
            data = await TryTakeValidData(source);
        }

        var collector = new BadLineCollector(null);
        var parser = new LineParser(data, collector);
        var lineNumber = await parser.GetMaxValueLineNumber();

        Console.WriteLine(lineNumber);

        PrintBadLines(collector);

        Console.ReadKey();
    }
}